#include <assert.h>
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/tcp.h>
#include <linux/udp.h>

#include "bpf_endian.h"
#include "bpf_helpers.h"
#include "nfv.h"
#define DEBUG_MAP PROXY_DEBUGS_MAP
#include "common_maps.c"
#include "maps.c"
#include "helpers.h"
#include "mpls.c"

/* ---------- sections --------*/

SEC("switch") int switch1(struct __sk_buff *skb) {
  int ret = 0;
  unsigned int hdr_cnt = 0, sid = 0;
  __u8 func = 0;
  __u16 args = 0;

  ret = mpls_traverse_stack(skb, &hdr_cnt, &sid, &func, &args);

  if (ret != BPF_CONTINUE) return ret;

  unsigned int func2 = 0 | func;
  unsigned int args2 = 0 | args;

  ret =
      save_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &hdr_cnt, &sid, &func2, &args2);
  if (ret) {
    bpf_debug_printk2(
        DEBUG_LOW,
        "[nfv_switch] Saving MPLS stack info failed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  /* BRANCH 0: MPLS stack has NETPROG headers, use FUNC to tail call a specific
   * NFV
   */

  if (func) {
    bpf_debug_printk2(DEBUG_LOW, "[nfv_switch] Tail call to netprog func=%d\n",
                      func);
    bpf_tail_call(skb, &FUNC_SWITCH_MAP, func);
  }

  /* BRANCH 1: Check if sr_proxy has been configured to handle packet's SID, if
   * true tail call sr_proxy
   */

  unsigned int *vp = 0;
  vp = bpf_map_lookup_elem(&PROXY_IN_SID_IF_MAP, &sid);
  // bpf_debug_printk2(DEBUG_LOW, "map loookup: key=%d res=%d\n", sid, vp);

  if (vp) {
    bpf_debug_printk2(DEBUG_LOW, "[nfv_switch] Tail call to sr_proxy, sid=%d\n",
                      sid);
    bpf_tail_call(skb, &FUNC_SWITCH_MAP, SR_PROXY_FUNC_ID);
  }

  // BRANCH3: Pass the execution to a default program
  bpf_debug_printk2(DEBUG_LOW, "[nfv_switch] Tail call to mpls_fw, sid=%d\n",
                    sid);
  bpf_tail_call(skb, &FUNC_SWITCH_MAP, MPLS_FW_FUNC_ID);

  // FINISH: None of tail_calls worked, dropping the packet
  bpf_debug_printk2(DEBUG_LOW,
                    "[nfv_switch] Tail call failed. Packet dropped.\n");
  return TC_ACT_SHOT;
}

static char _license[] SEC("license") = "GPL";
