#!/usr/bin/env python

# Based on: https://github.com/NetSys/bess/blob/2516e21348e33be5b65eb9ce451d3bdfce4d4a4b/bessctl/conf/samples/mpls_test.bess

import scapy.all as scapy
import sys

if (len(sys.argv) < 3):
    print "Usage: %s <if> <s|r> <ip|mpls>" % (sys.argv[0])
    sys.exit()

IFACE=sys.argv[1]
ACTION=sys.argv[2]
PACKET_TYPE=sys.argv[3]

# MPLS layer addition to scapy
class MPLS(scapy.Packet):
        name = "MPLS"
        fields_desc =  [
                scapy.BitField("label", 3, 20),
                scapy.BitField("experimental_bits", 0, 3),
                scapy.BitField("bottom_of_label_stack", 1, 1),
                scapy.ByteField("TTL", 255)
        ]
            
scapy.bind_layers(scapy.Ether, MPLS, type = 0x8847)
scapy.bind_layers(MPLS, MPLS, bottom_of_label_stack = 0)
scapy.bind_layers(MPLS, scapy.IP)

def cmp_packets(pkt1, pkt2):

    is_equal=True

    if pkt1.haslayer('Ether'):
        for ether_field in ('src', 'dst', 'type'):

            pkt1_field = getattr(pkt1.getlayer('Ether'), ether_field)
            pkt2_field = getattr(pkt2.getlayer('Ether'), ether_field)

            if pkt1_field != pkt2_field:
                print "Field %s differs!" % ether_field
                is_equal=False


    if pkt1.haslayer('MPLS'):

        """
        TODO: for now the test only compares the top label
        We need to traverse the whole mpls stack

        """

        for mpls_field in ('label', 'experimental_bits', 'bottom_of_label_stack', 'TTL'):

            pkt1_field = getattr(pkt1.getlayer('MPLS'), mpls_field)
            pkt2_field = getattr(pkt2.getlayer('MPLS'), mpls_field)

            if pkt1_field != pkt2_field:
                print "Field %s differs!" % mpls_field
                is_equal=False

    if pkt1.haslayer('IP'):

	"""
	 version= 4L
	 ihl= 5L
	 tos= 0x0
	 len= 38
	 id= 1
	 flags=
	 frag= 0L
	 ttl= 64
	 proto= udp
	 chksum= 0xb01c
	 src= 192.168.0.1
	 dst= 10.0.0.1
	"""
        for ip_field in ('src', 'dst', 'version', 'tos', 'proto', 'id'):

            pkt1_field = getattr(pkt1.getlayer('IP'), ip_field)
            pkt2_field = getattr(pkt2.getlayer('IP'), ip_field)

            if pkt1_field != pkt2_field:
                print "Field %s differs!" % ip_field
                is_equal=True



    return is_equal

def mk_packet(packet_type, payload_len=10):
            
    eth = scapy.Ether(src='4e:0e:2e:b7:1b:4f', dst='36:8d:c2:11:01:65')

    mpls0 = MPLS(label = 1012, TTL = 254, bottom_of_label_stack=0)
    mpls1 = MPLS(label = 1013, TTL = 254, bottom_of_label_stack=0)
    mpls_bos = MPLS(label = 0xDEAD, TTL = 255, bottom_of_label_stack=1)

    mpls = (mpls0/mpls1/mpls_bos)
    mpls_popped = (mpls1/mpls_bos)

    ip = scapy.IP(version=4,src='192.168.0.1', dst='10.0.0.1')
    udp = scapy.UDP(sport=10001, dport=10002)

    raw_payload=''

    for i in range(payload_len):
        num = i % 10
        raw_payload = raw_payload + str(num)

    payload = scapy.Raw(load=raw_payload)
    
    if packet_type == 'mpls':
        packet = (eth/mpls/ip/udp/payload)
    elif packet_type == 'mpls_popped':
        packet = (eth/mpls_popped/ip/udp/payload)
    elif packet_type == 'ip':
        packet = (eth/ip/udp/payload)
    else:
        raise Exception

    return packet

test_packet = mk_packet(PACKET_TYPE)
rcvd_packet=''

if ACTION == 's':
    #test_packet.show()
    scapy.sendp(test_packet, count=1, iface=IFACE, verbose=False)
elif ACTION == 'r':
    
    if test_packet.haslayer('MPLS'):
        second_label = mk_packet('mpls').getlayer('MPLS')[1].label
        tcpdump_filter = "!ip6 and mpls %s" % second_label
    else:
        tcpdump_filter = "!ip6 and %s" % PACKET_TYPE.split('_')[0]

    rcvd_packet = scapy.sniff(count=1, timeout=5, iface=IFACE, filter=tcpdump_filter)
    #print(rcvd_packet[0].show())

    try:
        rcvd_packet = rcvd_packet[0]
    except IndexError:
        #no packet was received
        print "ERR"
        sys.exit(1)

    if cmp_packets(test_packet, rcvd_packet):
        print "OK"
        sys.exit()
    else:
        print "ERR"
        sys.exit(1)

else:
    print "Unknown action"
    sys.exit(1)
