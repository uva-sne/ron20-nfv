source ./common.sh

# Test MPLS decap
# Send MPLS encapsulated packet on ENCAP_IN_IF and expect it to arrive on
# ENCAP_OUT_IF but without MPLS headers

#associating label 1012 with interface $DECAP_REDIR_TO_IF
set_decap_redir_if

#expect an IP packet _without_ MPLS header
${NSEXEC} python packet.py ${IN_NS_IFACE} r ip > $RESULT_FILE &
rcvr_pid=$!

trap "kill $rcvr_pid" SIGINT

sleep 2

#send a packet with MPLS header
python packet.py ${MPLS_IFACE_EXT} s mpls

wait $rcvr_pid

grep -qw OK $RESULT_FILE
rc=$?

rm -f $RESULT_FILE

if [[ $rc -eq 0 ]];then

	echo "OK"
	exit 0

else
	echo "ERR"
	exit 1
fi
