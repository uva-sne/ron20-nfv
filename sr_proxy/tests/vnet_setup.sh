#!/bin/sh
# This test sets up the following topo:
#   +-----------+
#   |           |
#   | testns1   |
#   |           |
#   | veth1     |
#   +-----------+
#         |
#         |
#         |
#  +------------+
#  |  veth0  (SEC sr_proxy_encap, clsact/ingress)
#  |            | 
#  |            |
#  |       io0 +------+ io1
#  |       ^ (SEC sr_proxy_decap, clsact/egress)
#  |            |
#  +------------+
#

#create virtual links
ip link add dev veth0 type veth peer name veth1

ip link add dev io0 type veth peer name io1

#add ns
NSNAME="testns1"
NSEXEC="ip netns exec $NSNAME"

ip netns add $NSNAME

#move link 1 to ns
for if in veth1;do
	ip link set $if netns $NSNAME
done

#adjust link in NS
for if in veth1 lo;do
	$NSEXEC ip link set $if up
	$NSEXEC sysctl -w net.ipv4.conf.${if}.rp_filter=0
done

#adjust links in the host
for if in veth0 io{0,1};do
	ip link set $if up
	sysctl -w net.ipv4.conf.${if}.rp_filter=0
done

#setup a default route in NS
$NSEXEC arp -s 192.168.1.1 02:05:86:71:b2:0f
$NSEXEC ip route add default via 192.168.1.1
