source ./common.sh

# Test full proxy behaviour i.e. decap and caching of MPLS headers, receiving
# packets by the proxied app and sending back to get re-encapsulated in MPLS

## MAP SETUP

set_decap_redir_if
set_encap_if_sid
set_out_mpls_if

## MAIN

#run service which our proxy is handling
proxy_pid=$(run_proxied_svc)

#expect an IP packet with MPLS header, just top label popped
python packet.py ${MPLS_IFACE_EXT} r mpls_popped > $RESULT_FILE &
rcvr_pid=$!

trap "kill $rcvr_pid $proxy_pid" SIGINT

sleep 2

#send a packet with MPLS header
python packet.py ${MPLS_IFACE_EXT} s mpls

#when a packet was received stop the service
wait $rcvr_pid;kill $proxy_pid

grep -qw OK $RESULT_FILE
rc=$?

rm -f $RESULT_FILE

if [[ $rc -eq 0 ]];then

	echo "OK"
	exit 0

else
	echo "ERR"
	exit 1
fi
