source ./common.sh

# Test MPLS decap
# Send MPLS encapsulated packet on ENCAP_IN_IF and expect it to arrive on
# ENCAP_OUT_IF but without MPLS headers

#configure outgoing MPLS interface id
set_out_mpls_if

#associate $ENCAP_OUT_IF with label 1012
set_encap_if_sid

#expect an IP packet _without_ MPLS header
python packet.py ${MPLS_IFACE_EXT} r mpls_popped > $RESULT_FILE &
rcvr_pid=$!

trap "kill $rcvr_pid" SIGINT

sleep 2

#send a packet with MPLS header
$NSEXEC python packet.py ${IN_NS_IFACE} s ip

wait $rcvr_pid

grep -qw OK $RESULT_FILE
rc=$?

rm -f $RESULT_FILE

if [[ $rc -eq 0 ]];then

	echo "OK"
	exit 0

else
	echo "ERR"
	exit 1
fi
