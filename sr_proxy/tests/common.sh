NS_NAME=testns1
NSEXEC="ip netns exec ${NS_NAME}"
RESULT_FILE=$(tempfile -d /tmp -p $0)

HOST_NS_IFACE=veth0
#just for the purpose of documentation
IN_NS_IFACE=veth1
MPLS_IFACE=io1
MPLS_IFACE_EXT=io0

SID_B_ENDIAN="0xf4 0x03 0x00 0x00"
SID_L_ENDIAN="0x00 0x00 0x03 0xf4"

function set_debug(){
	level=$1
	echo "Setting debug to level=$level" >&2
	bpftool map update pinned /sys/fs/bpf/tc/globals/PROXY_DEBUGS_MAP key hex 0x00 0x00 0x00 0x00 value hex 0x0${level}
}

function run_proxied_svc(){
	#socat emulates the service which does nothing but putting packets from one interface to another
	$NSEXEC ip netns exec testns1 socat -v UDP-RECVFROM:10002,fork,reuseaddr EXEC:cat > /tmp/socat.out 2>&1 &
	echo $!
}

function get_if_idx(){
	if_name=$1
	grep ${if_name}$ /proc/net/if_inet6  | awk '{print "0x"$2}'
}

function set_decap_redir_if(){
	#associating label 1012 with interface $DECAP_REDIR_TO_IF
	bpftool map update pinned /sys/fs/bpf/tc/globals/PROXY_IN_SID_IF_MAP key hex ${SID_B_ENDIAN} value hex $(get_if_idx ${HOST_NS_IFACE}) 0x00 0x00 0x00
}

function set_encap_if_sid(){
	#associate $ENCAP_OUT_IF with label 1012
	bpftool map update pinned /sys/fs/bpf/tc/globals/PROXY_OUT_SID_IF_MAP key hex $(get_if_idx ${HOST_NS_IFACE}) 0x00 0x00 0x00 value hex ${SID_B_ENDIAN}
}

function set_out_mpls_if(){
	#configure outgoing MPLS interface id
	bpftool map update pinned /sys/fs/bpf/tc/globals/PROXY_OUT_MPLS_IF key hex 0x00 value hex $(get_if_idx ${MPLS_IFACE_EXT}) 
}

set_debug 2
