#include <assert.h>
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/tcp.h>
#include <linux/udp.h>

#include "bpf_endian.h"
#include "bpf_helpers.h"
#include "helpers.h"
#define DEBUG_MAP PROXY_DEBUGS_MAP
#include "common_maps.c"
#include "maps.c"
#include "mpls.c"

static int __always_inline get_out_if_idx() {
  // key is always 0
  __u8 k = 0;

  unsigned int *vp = 0;
  vp = bpf_map_lookup_elem(&PROXY_OUT_MPLS_IF, &k);

  if (vp) {
    __u8 if_idx = *vp;
    return if_idx;
  } else {
    return 0;
  }
}

SEC("proxy_in") int sr_proxy_decap1(struct __sk_buff *skb) {
  int i = 0;
  int label_len = sizeof(struct mpls_hdr);

  unsigned int MPLS_LABEL_CNT;
  unsigned int MPLS_SID = 0;
  __u8 func;
  __u16 args;

  struct mpls_hdrs cached_mpls_hdrs = {0, 0};

#ifdef STANDALONE_MODE

  int ret = mpls_traverse_stack(skb, &MPLS_LABEL_CNT, &MPLS_SID, &func, &args);

#else

  int ret = get_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &MPLS_LABEL_CNT, &MPLS_SID,
                                &func, &args);

#endif

  if (ret != BPF_CONTINUE) return ret;

  bpf_debug_printk2(DEBUG_LOW, "[proxy_in] depth=%d sid=%d\n", MPLS_LABEL_CNT,
                    MPLS_SID);

  // offset should include skipping the ethernet hdr + the first MPLS hdr (which
  // is going to be popped)
  int off = sizeof(struct ethhdr) + sizeof(struct mpls_hdr);

  int len = MPLS_LABEL_CNT * sizeof(struct mpls_hdr);

#ifndef MPLS_PHP

  cached_mpls_hdrs.cnt = MPLS_LABEL_CNT - 1;

#else

  cached_mpls_hdrs.cnt = MPLS_LABEL_CNT;

#endif

  /*TODO: we need to fix the logic of a proxy caching.
  Currently there is no awareness of netrprog headers, also returned number of
  headers is not taking account the top header which is always popped
  */

  int err;

  void *data = (void *)(long)skb->data;
  void *data_end = (void *)(long)skb->data_end;
  struct ethhdr *eth = (struct ethhdr *)(data);

  if ((void *)(eth + 1) > data_end) return TC_ACT_SHOT;

  for (int i = 0; i < MPLS_MAX_STACK_DEPTH; i++) {
    /*
     * This seems suboptimal from C coding point of view but this is the
     * only way I found to make it accepted by the verifier
     */

    if (i == MPLS_LABEL_CNT) break;

    struct mpls_hdr *mpls =
        (struct mpls_hdr *)((void *)(eth + 1) + (i * MPLS_HLEN));

    if ((void *)(mpls + MPLS_HLEN + 1) > data_end) return TC_ACT_SHOT;

#ifndef MPLS_PHP

    if (i > 0) {
      bpf_memcpy(&cached_mpls_hdrs.hdr[i - 1], &mpls->entry, MPLS_HLEN);
    }
#else
    bpf_memcpy(&cached_mpls_hdrs.hdr[i], &mpls->entry, MPLS_HLEN);
#endif

    /*
    The intended way here was using a single function call to read all required
    MPLS labels *at once*. Unfortunatelly, the verifier is very picky so for now
    I stick with direct-access approach i.e. bpf_memcpy()
    TODO: experiment with better boundary checks i.e. (void *)(mpls +
    MPLS_HLEN*MPLS_LABEL_CNT + 1) > data_end

    err = bpf_skb_load_bytes(skb, off, &cached_mpls_hdrs.hdr, len);
    if (err) {
      bpf_debug_printk2(DEBUG_HIGH, "[proxy_in] bpf_skb_load_bytes err=%d\n",
    err);
    }
    */
  }

  bpf_map_update_elem(&PROXY_LABELS_MAP, &MPLS_SID, &cached_mpls_hdrs,
                      BPF_ANY);

  // pop all labels
  pop_mpls_labels(skb, MPLS_LABEL_CNT, MPLS_LABEL_CNT);

  // return bpf_redirect_map(&PROXY_IN_SID_IF_MAP, &if_idx, BPF_F_INGRESS);

  unsigned int *vp0 = 0;

  vp0 = bpf_map_lookup_elem(&PROXY_IN_SID_IF_MAP, &MPLS_SID);

  if (vp0) {
    unsigned int if_idx = *vp0;
    bpf_debug_printk2(DEBUG_LOW, "[proxy_in] Sending SID=%d to if idx=%d\n",
                      MPLS_SID, if_idx);

    err = bpf_skb_change_type(skb, PACKET_HOST);
    if (err) {
      bpf_debug_printk2(DEBUG_HIGH, "[proxy_in] bpf_skb_change_type err=%d\n",
                        err);
    }

    // clear UDP checksum, TODO: refractor
    __u16 udp_cksum = 0;
    bpf_skb_store_bytes(skb, 40, &udp_cksum, 2, BPF_F_INVALIDATE_HASH);

    int ret = bpf_redirect(if_idx, 0);
    bpf_debug_printk2(DEBUG_HIGH, "[proxy_in] bpf_redirect ret=%d\n", ret);
    return ret;

  } else {
    bpf_debug_printk2(
        DEBUG_LOW,
        "[proxy_in] if_idx lookup failed for if SID=%d. Packet dropped.\n",
        MPLS_SID);
    return TC_ACT_SHOT;
  }
}

SEC("proxy_out") int proxy_out1(struct __sk_buff *skb) {
  void *data_end = (void *)(long)skb->data_end;
  void *data = (void *)(long)skb->data;

  struct ethhdr *eth = (struct ethhdr *)(data);

  if ((void *)(eth + 1) > data_end) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[proxy_out] Socket buffer struct was malformed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  if (eth->h_proto != bpf_htons(ETH_P_IP) &&
      eth->h_proto != bpf_htons(ETH_P_ARP)) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[proxy_out] Unexpected ethertype (0x%x). Packet dropped.\n",
        bpf_ntohs(eth->h_proto));
    return TC_ACT_SHOT;
  }

  unsigned int *vp0 = 0;
  unsigned int sid = 0;
  unsigned int if_idx = skb->ifindex;

  // lookup ifindex SID
  vp0 = bpf_map_lookup_elem(&PROXY_OUT_SID_IF_MAP, &if_idx);

  if (vp0) {
    sid = *vp0;
    bpf_debug_printk2(DEBUG_HIGH, "[proxy_out] ifindex=%d SID=%d\n",
                      skb->ifindex, sid);
  } else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[proxy_out] SID lookup failed for if idx=%d\n",
                      skb->ifindex);
  }

  // lookup cached labels for SID
  struct mpls_hdrs *vp = 0;
  vp = bpf_map_lookup_elem(&PROXY_LABELS_MAP, &sid);
  struct mpls_hdrs cached_hdrs = {0, 0};

  if (vp) {
    /*
     * Just as a reminder:
     * - we do not want to swap mac adresses, this is a PROXY the OS will
     *   handle picking the right addresses
     * - we do not pop the top mpls header, [proxy_in] caches everything but
     *   the top label so we can simply apply what is in the cache
     */

    cached_hdrs = *vp;

    bpf_debug_printk2(DEBUG_HIGH, "[proxy_out] Cached headers count=%d\n",
                      cached_hdrs.cnt);

    /*
     * This is a workaround to get around verifier errors. The code below is
     * basically assigning cached_hdrs.cnt with its own value but in a static
     * manner
     */

    unsigned int cached_hdrs_cnt = 1;
    for (int i = 1; i < MPLS_MAX_STACK_DEPTH; ++i) {
      if (cached_hdrs.cnt == i) {
        cached_hdrs_cnt = i;
        break;
      }
    }
    cached_hdrs.cnt = cached_hdrs_cnt;
    push_mpls_labels2(skb, &cached_hdrs);

    // get outgoing interface ID
    int out_if_idx = get_out_if_idx();

    if (out_if_idx) {
      int ret = bpf_redirect(out_if_idx, 0);
      bpf_debug_printk2(DEBUG_HIGH,
                        "[proxy_out] bpf_redirect ret=%d if_idx=%d\n", ret,
                        out_if_idx);

      return ret;
    } else {
      bpf_debug_printk2(
          DEBUG_LOW,
          "[proxy_out] No outgoing MPLS iface configured. Packet dropped.\n");
      return TC_ACT_SHOT;
    }

  } else {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[proxy_out] Cache lookup failed (key: %llx). Packet dropped.\n", sid);
    return TC_ACT_SHOT;
  }
}

static char _license[] SEC("license") = "GPL";
