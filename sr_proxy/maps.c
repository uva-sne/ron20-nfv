#include "mpls.h"
#include "nfv.h"

/*
 * For caching these types of mappings (which could change dynamically) LRU
 * maps could be used to keep map content accurate
 *
 * TODO: For speed those maps should be probably arrays
 */

#define __MPLS_HDRS_STRUCT_SIZE \
  sizeof(unsigned int) + MPLS_MAX_STACK_DEPTH * sizeof(unsigned int)

struct bpf_elf_map SEC("maps") PROXY_LABELS_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(unsigned int),       // SID
    .size_value = __MPLS_HDRS_STRUCT_SIZE,  // Labels cached towards this SID
    .pinning = PIN_GLOBAL_NS,
    .max_elem = MAX_NFV_CNT,
};

struct bpf_elf_map SEC("maps") PROXY_IN_SID_IF_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(unsigned int),    // SID
    .size_value = sizeof(unsigned int),  // Interface ID leading towards an NFV
    .pinning = PIN_GLOBAL_NS,
    .max_elem = MAX_NFV_CNT,
};

struct bpf_elf_map SEC("maps") PROXY_OUT_SID_IF_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(
        unsigned int),  // Interface ID of the traffic will come out of an NFV
    .size_value = sizeof(unsigned int),  // SID
    .pinning = PIN_GLOBAL_NS,
    .max_elem = MAX_NFV_CNT,
};

struct bpf_elf_map SEC("maps") PROXY_OUT_MPLS_IF = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = 1,    // 1 byte
    .size_value = 1,  // 1 byte
    .pinning = PIN_GLOBAL_NS,
    .max_elem = 1,
};
