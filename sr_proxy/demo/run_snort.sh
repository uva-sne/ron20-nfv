docker run -d --name snort --rm -v `pwd`/rules:/etc/snort/rules linton/snort-base snort -i eth0 -c /etc/snort/etc/snort.conf -A full

#make the container NS visible to 'ip netns'
pid=$(docker inspect -f '{{.State.Pid}}' snort)
mkdir -p /var/run/netns/
ln -sfT /proc/$pid/ns/net /var/run/netns/snort


SNORT_IF_ID=$(ip link show dev $(brctl show docker0 | sed -e 1d | perl -alne 'print $F[-1]' | head -1) | grep -Eo '^[0-9]+')

#update value for FUNC=0x8 (IDS)
bpftool map update pinned /sys/fs/bpf/tc/globals/PROXY_IN_SID_IF_MAP key hex 0x8 0x0 0x0 0x0 value ${SNORT_IF_ID} 0 0 0

echo "Tailing /var/log/snort/alert.."
docker exec -it snort tail -f /var/log/snort/alert
