import scapy.all as scapy

EXP_PAYLOAD='😞'
NEW_PAYLOAD='😀'

while True:
	rcvd_packets = scapy.sniff(count=1, iface="eth0", filter="udp or tcp")
	p = rcvd_packets[0]

	payload = p.getlayer('UDP').load.decode()
	if payload == EXP_PAYLOAD:
		p.getlayer('UDP').load=NEW_PAYLOAD
		#p.getlayer('UDP').len=len(NEW_PAYLOAD)

		#p.show()
		scapy.sendp(p, count=1, iface="eth0", verbose=True)
	else:
		print("payload %s" % payload)
		continue
