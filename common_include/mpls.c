#ifndef MPLS_C
#define MPLS_C
#include "mpls.h"

/*
 * check if the protocol type is either MPLS unicast or MPLS multicast
 */
static inline bool is_eth_p_mpls(unsigned short eth_type) {
  return eth_type == bpf_htons(ETH_P_MPLS_UC) ||
         eth_type == bpf_htons(ETH_P_MPLS_MC);
}

/*
 * check if the protocol type is either MPLS
 */
static inline bool is_ip_p_mpls(unsigned short ip_type) {
  return ip_type == bpf_htons(IPPROTO_MPLS);
}

/*
 * test whether mpl header is the bottom of the stack.
 * @param hdr the mpls header to test
 */
static inline bool is_mpls_entry_bos(struct mpls_hdr *hdr) {
  return hdr->entry & bpf_htonl(MPLS_LS_S_MASK);
}

/*
 * encode the values into the network header.
 * @label the 20bits of MPLS label
 * @ttl time to live
 * @tc traffic class
 * @bos true/false if label is the bottom of the stack
 */
static inline struct mpls_hdr mpls_encode(unsigned int label, unsigned int ttl,
                                          unsigned int tc, bool bos) {
  struct mpls_hdr result;
  result.entry =
      // we need to convert from CPU endian to network endian
      bpf_htonl((label << MPLS_LS_LABEL_SHIFT) | (tc << MPLS_LS_TC_SHIFT) |
                (bos ? (1 << MPLS_LS_S_SHIFT) : 0) |
                (ttl << MPLS_LS_TTL_SHIFT));
  return result;
}

/*
 * decode the header into a friendlier struct for easy access.
 * @hdr the mpls header
 */
static inline struct mpls_entry_decoded mpls_entry_decode(
    struct mpls_hdr *hdr) {
  struct mpls_entry_decoded result;
  // we need to convert from network endian to host endian
  unsigned int entry = bpf_ntohl(hdr->entry);

  result.label = (entry & MPLS_LS_LABEL_MASK) >> MPLS_LS_LABEL_SHIFT;
  result.ttl = (entry & MPLS_LS_TTL_MASK) >> MPLS_LS_TTL_SHIFT;
  result.tc = (entry & MPLS_LS_TC_MASK) >> MPLS_LS_TC_SHIFT;
  result.bos = (entry & MPLS_LS_S_MASK) >> MPLS_LS_S_SHIFT;

  return result;
}

int __always_inline push_mpls_labels2(struct __sk_buff *skb,
                                      struct mpls_hdrs *cached_hdrs) {
  __u16 etherype_mpls = __bpf_constant_htons(ETH_P_MPLS_UC);

  unsigned int off = sizeof(struct ethhdr) - sizeof(etherype_mpls);
  unsigned int off_mpls = sizeof(struct ethhdr);
  unsigned int pushed_labels_size = cached_hdrs->cnt * MPLS_HLEN;

  int err = 0;

  err = bpf_skb_adjust_room(skb, pushed_labels_size, BPF_ADJ_ROOM_MAC,
                            BPF_F_ADJ_ROOM_ENCAP_L2(pushed_labels_size));

  if (err) {
    bpf_debug_printk2(DEBUG_HIGH, "[mpls_push] skb_adjust_room err=%d\n", err);
  }

  // change ethertype
  err = bpf_skb_store_bytes(skb, off, &etherype_mpls, sizeof(etherype_mpls),
                            BPF_F_INVALIDATE_HASH);
  if (err) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[mpls_push] [ethertype_chnge] bpf_skb_store_bytes err=%d\n", err);
  }

  bpf_debug_printk2(
      DEBUG_HIGH, "[mpls_push] MPLS hdr offset=%d, pushed labels size is %dB\n",
      off_mpls, pushed_labels_size);

  err = bpf_skb_store_bytes(skb, off_mpls, cached_hdrs->hdr, pushed_labels_size,
                            0);
  if (err) {
    bpf_debug_printk2(
        DEBUG_HIGH, "[mpls_push] [lbl_push] bpf_skb_store_bytes err=%d\n", err);
  }
}

void __always_inline pop_mpls_labels(struct __sk_buff *skb, unsigned int cnt,
                                     unsigned int stack_depth) {
  __u16 etherype_ipv4 = __bpf_constant_htons(ETH_P_IP);

  int ethertype_off = sizeof(struct ethhdr) - sizeof(etherype_ipv4);
  int popped_labels_size = cnt * sizeof(struct mpls_hdr);

  int err = 0;

  if (cnt == stack_depth) {
    // change ethertype when all MPLS labels are popped
    err = bpf_skb_store_bytes(skb, ethertype_off, &etherype_ipv4,
                              sizeof(etherype_ipv4), BPF_F_INVALIDATE_HASH);
    bpf_debug_printk2(DEBUG_HIGH, "[mpls_pop] Changing ethertype to ipv4\n");
  }

  if (err) {
    bpf_debug_printk2(
        DEBUG_HIGH, "[mpls_pop] [ethertype_chnge] bpf_skb_store_bytes err=%d\n",
        err);
  }

  err = bpf_skb_adjust_room(skb, -popped_labels_size, BPF_ADJ_ROOM_MAC, 0);

  if (err) {
    bpf_debug_printk2(DEBUG_HIGH, "[mpls_pop] skb_adjust_room err=%d\n", err);
  }
}

int __always_inline swap_mac_addrs_da(struct __sk_buff *skb) {
  void *data_end = (void *)(long)skb->data_end;
  void *data = (void *)(long)skb->data;

  struct ethhdr *eth = (struct ethhdr *)(data);

  if ((void *)(eth + 1) > data_end) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[mac_swp] Socket buffer struct was malformed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  __u8 src_mac[ETH_ALEN];
  __u8 dst_mac[ETH_ALEN];
  bpf_memcpy(src_mac, eth->h_source, ETH_ALEN);
  bpf_memcpy(dst_mac, eth->h_dest, ETH_ALEN);

  bpf_memcpy(eth->h_dest, src_mac, ETH_ALEN);
  bpf_memcpy(eth->h_source, dst_mac, ETH_ALEN);
}

int __always_inline push_sr_netprog_labels(struct __sk_buff *skb, int loc,
                                           __u8 func, __u16 args) {
  /*Create MPLS_LABEL_EXTENSION and MPLS_LABEL_SR_NETPROG headers*/
  struct mpls_hdr mpls_xl =
      mpls_encode(MPLS_LABEL_EXTENSION, MPLS_DEF_TTL, 0, 0);
  struct mpls_hdr mpls_netprog_exp =
      mpls_encode(MPLS_LABEL_SR_NETPROG, MPLS_DEF_TTL, 0, 0);

  /*Create top-of-the-stack header containing location of an NFV (loc)*/
  struct mpls_hdr mpls_netprog_loc = mpls_encode(loc, MPLS_DEF_TTL, 0, 0);

  /*Create header containing contcatenated SR NFV function and arguments*/

  int func_args = (func << NETPROG_ARGS_LEN | args);

  struct mpls_hdr mpls_netprog_func_args =
      mpls_encode(func_args, MPLS_DEF_TTL, 0, 0);

  // TODO: Figure out if all what we want ends up in func_args. It seems not.

  /* Place all created headers inside of mpls_hdrs structure*/
  struct mpls_hdrs mpls_netprog_stack;
  mpls_netprog_stack.cnt = MPLS_NETPROG_STACK_DEPTH;
  bpf_memcpy(&mpls_netprog_stack.hdr[0], &mpls_netprog_loc, MPLS_HLEN);
  bpf_memcpy(&mpls_netprog_stack.hdr[1], &mpls_xl, MPLS_HLEN);
  bpf_memcpy(&mpls_netprog_stack.hdr[2], &mpls_netprog_exp, MPLS_HLEN);
  bpf_memcpy(&mpls_netprog_stack.hdr[3], &mpls_netprog_func_args, MPLS_HLEN);

  push_mpls_labels2(skb, &mpls_netprog_stack);
}

int __always_inline mpls_traverse_stack(struct __sk_buff *skb, int *hdr_cnt,
                                        unsigned int *sid, __u8 *func,
                                        __u16 *args) {
  void *data_end = (void *)(long)skb->data_end;
  void *data = (void *)(long)skb->data;

  // The packet starts with the ethernet header, so let's get that going:
  struct ethhdr *eth = (struct ethhdr *)(data);

  /*
   * Now, we can't just go "eth->h_proto", that's illegal.  We have to
   * explicitly test that such an access is in range and doesn't go
   * beyond "data_end" -- again for the verifier.
   * The eBPF verifier will see that "eth" holds a packet pointer,
   * and also that you have made sure that from "eth" to "eth + 1"
   * is inside the valid access range for the packet.
   */
  if ((void *)(eth + 1) > data_end) {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[mpls_parse] [ethhdr] Socket buffer struct was "
                      "malformed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  /* Verify Ethertype is MPLS*/
  if (eth->h_proto != bpf_htons(ETH_P_MPLS_UC)) {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[mpls_parse] Ethertype is not MPLS_UC (0x%x). "
                      "Packet ignored.\n",
                      bpf_ntohs(eth->h_proto));
    return TC_ACT_OK;
  }

  /* Iterate over MPLS labels */
  int i = 0;
  bool ext_lbl_prsnt = false;
  bool netprog_lbl_prsnt = false;

#ifndef BPF_LOOP_SUPPORTED
#define pragma unroll(full)
#endif
  for (i = 0; i < MPLS_MAX_STACK_DEPTH; i++) {
    bpf_debug_printk2(DEBUG_HIGH, "[mpls_parse] Label #%d\n", i);

    struct mpls_hdr *mpls =
        (struct mpls_hdr *)((void *)(eth + 1) + (i * MPLS_HLEN));

    if ((void *)(mpls + 1) > data_end) {
      bpf_debug_printk2(DEBUG_HIGH,
                        "[mpls_parse] [mpls_hdr] Socket buffer struct was "
                        "malformed. Packet dropped.\n");
      return TC_ACT_SHOT;
    }

    struct mpls_entry_decoded mpls_decoded = mpls_entry_decode(mpls);

    bpf_debug_printk2(DEBUG_HIGH, "[mpls_parse] Decoded MPLS label: %d\n",
                      mpls_decoded.label);

    /*
     * IMPORTANT TODO: currently once the BOS label hdr is reached we end
     * parsing. For netprog it might be that the last label (func+args)
     * is also BOS, then we'd not parse it
     */

    if (is_mpls_entry_bos(mpls)) {
      bpf_debug_printk2(DEBUG_HIGH, "[mpls_parse] MPLS S=1 label reached\n");

      /*
       * This is the point where parsing of a correct MPLS stack should end
       *
       * If the _top_ label was BOS. Drop the packet.
       * Otherwise accept it for futher processing.
       */

      if (i == 0)
        return TC_ACT_SHOT;
      else {
        *hdr_cnt = i + 1;
        bpf_debug_printk2(DEBUG_LOW, "[mpls_parse] MPLS stack depth: %d\n",
                          *hdr_cnt);
        return BPF_CONTINUE;
      }
    }

    /*
     * First label and not BOS
     */

    if (i == 0) {
      *sid = mpls_decoded.label;
      bpf_debug_printk2(DEBUG_LOW, "[mpls_parse] SID (top label): %d\n", *sid);
    }
    /*
     SR PROG start
     */

    else if (i == 1 && mpls_decoded.label == MPLS_LABEL_EXTENSION) {
      ext_lbl_prsnt = true;
      bpf_debug_printk2(DEBUG_HIGH, "[mpls_parse] MPLS XL label found\n");
    } else if (ext_lbl_prsnt && i == 2 &&
               mpls_decoded.label == MPLS_LABEL_SR_NETPROG) {
      netprog_lbl_prsnt = true;
      bpf_debug_printk2(DEBUG_HIGH, "[mpls_parse] MPLS NETPROG label found\n");
    } else if (ext_lbl_prsnt && netprog_lbl_prsnt && i == 3) {
      *func = (mpls_decoded.label & 0xF0000) >> NETPROG_ARGS_LEN;
      *args = (mpls_decoded.label & 0x0FFFF);

      bpf_debug_printk2(DEBUG_HIGH,
                        "[mpls_parse] func=%lld args=%lld raw=0x%llx\n", *func,
                        *args, mpls_decoded.label);
    }

    /*
     SR PROG end
     */

    /*
     * Handling the edge case. Last run of the loop but still not a BOS
     */

    if (unlikely(i == MPLS_MAX_STACK_DEPTH - 1)) {
      bpf_debug_printk2(
          DEBUG_HIGH,
          "[mpls_parse] MPLS max stack depth limit reached. Packet dropped.\n");
      return TC_ACT_SHOT;
    }
  }
}

static int __always_inline get_mpls_stack_info(struct bpf_map *map,
                                               unsigned int *stack_depth,
                                               unsigned int *top_label,
                                               __u8 *netprog_func,
                                               __u16 *netprog_args) {
  unsigned int key;
  unsigned int *value_ptr = 0;

  key = MPLS_INFO_STACK_DEPTH_IDX;
  value_ptr = bpf_map_lookup_elem(map, &key);

  if (value_ptr)
    *stack_depth = *value_ptr;
  else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[get_mpls_info] bpf_map_lookup_elem %s NULL\n", key);
    return TC_ACT_SHOT;
  }

  key = MPLS_INFO_SID_IDX;
  value_ptr = bpf_map_lookup_elem(map, &key);

  if (value_ptr)
    *top_label = *value_ptr;
  else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[get_mpls_info] bpf_map_lookup_elem %s NULL\n", key);
    return TC_ACT_SHOT;
  }

  key = MPLS_INFO_NETPROG_FUNC_IDX;
  value_ptr = bpf_map_lookup_elem(map, &key);

  if (value_ptr)
    // type convertion, TODO: Verify this works as expected
    *netprog_func = *value_ptr;
  else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[get_mpls_info] bpf_map_lookup_elem %s NULL\n", key);
    return TC_ACT_SHOT;
  }

  key = MPLS_INFO_NETPROG_ARGS_IDX;
  value_ptr = bpf_map_lookup_elem(map, &key);

  if (value_ptr)
    // type convertion, TODO: Verify this works as expected
    *netprog_args = *value_ptr;
  else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[get_mpls_info] bpf_map_lookup_elem %s NULL\n", key);
    return TC_ACT_SHOT;
  }

  return BPF_CONTINUE;
}

static int __always_inline save_mpls_stack_info(struct bpf_map *map,
                                                unsigned int *stack_depth,
                                                unsigned int *top_label,
                                                unsigned int *netprog_func,
                                                unsigned int *netprog_args) {
  unsigned int key;
  int err, ultimate_rc;

  key = MPLS_INFO_STACK_DEPTH_IDX;
  err = bpf_map_update_elem(map, &key, stack_depth, BPF_ANY);
  ultimate_rc = err;

  if (err)
    bpf_debug_printk2(DEBUG_HIGH, "[nfv_switch] bpf_map_update_elem err=%d\n",
                      err);

  key = MPLS_INFO_SID_IDX;
  err = bpf_map_update_elem(map, &key, top_label, BPF_ANY);
  ultimate_rc = ultimate_rc | err;

  if (err)
    bpf_debug_printk2(DEBUG_HIGH, "[nfv_switch] bpf_map_update_elem err=%d\n",
                      err);

  key = MPLS_INFO_NETPROG_FUNC_IDX;
  err = bpf_map_update_elem(map, &key, netprog_func, BPF_ANY);
  ultimate_rc = ultimate_rc | err;

  if (err)
    bpf_debug_printk2(DEBUG_HIGH, "[nfv_switch] bpf_map_update_elem err=%d\n",
                      err);

  key = MPLS_INFO_NETPROG_ARGS_IDX;
  err = bpf_map_update_elem(map, &key, netprog_args, BPF_ANY);
  ultimate_rc = ultimate_rc | err;

  if (err)
    bpf_debug_printk2(DEBUG_HIGH, "[nfv_switch] bpf_map_update_elem err=%d\n",
                      err);

  return ultimate_rc;
}

#endif  // MPLS_C
