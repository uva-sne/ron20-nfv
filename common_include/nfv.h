#ifndef NFV_H
#define NFV_H

#define MAX_NFV_CNT 8

#define SR_PROXY_FUNC_ID 0
#define MPLS_FW_FUNC_ID 1
#define MIRROR_FUNC_ID 4
#define NFV_SWITCH_FUNC_ID 7

struct bpf_elf_map SEC("maps") FUNC_SWITCH_MAP = {
    .type = BPF_MAP_TYPE_PROG_ARRAY,
    .size_key = 4,
    .size_value = 4,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = MAX_NFV_CNT,
};

#endif  // NFV_H
