#ifndef COMMON_MAPS_C
#define COMMON_MAPS_C

/* 
 * DEBUG_MAP is a macro which gets defined to a program specific debug_map name
 * before including this file
 */

struct bpf_elf_map SEC("maps") DEBUG_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(unsigned int),
    .size_value = 1,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = 1,
};

/* This array is meant to hold the return argument of mpls_traverse_stack()
 * function.
 * The purpose is to avoid running this function twice, once in this program
 * and later in a called NFV.
 */

struct bpf_elf_map SEC("maps") FUNC_MPLS_INFO_MAP = {
    .type = BPF_MAP_TYPE_ARRAY,
    .size_key = 4,
    .size_value = 4,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = 4,
};

#endif //COMMON_MAPS_C
