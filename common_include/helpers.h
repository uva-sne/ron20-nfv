/**
 * A collection of useful definitions when writing eBPF.
 * Some of these were taken from
 * https://github.com/iovisor/bcc/blob/master/src/cc/export/helpers.h
 */

#ifndef HELPERS_H
#define HELPERS_H

#include <stdbool.h>
#include "bpf_helpers.h"

#define bpf_memcpy __builtin_memcpy

/**
 * Aside from BPF helper calls and BPF tail calls, the BPF instruction did not
 * arbitrary support functions -- as a result all functions need the inline
 * macro. Starting with Linux kernel 4.16 and LLVM 6.0 this restriction got
 * lifted. The typical inline keyword is only a hint whereas this is definitive.
 */
#define forced_inline __attribute__((always_inline))

/*
 * helper macro to place programs, maps, license in
 * different sections in elf_bpf file. Section names
 * are interpreted by elf_bpf loader
 */
#define SEC(NAME) __attribute__((section(NAME), used))

/*
 * helper macro to make it simpler to print trace messages to
 * bpf_trace_printk.
 * ex. bpf_printk("BPF command: %d\n", op);
 * you can find the output in /sys/kernel/debug/tracing/trace_pipe
 * however it will collide with any othe rrunning process.
 */
#define bpf_printk(fmt, ...)                                   \
  ({                                                           \
    char ____fmt[] = fmt;                                      \
    bpf_trace_printk(____fmt, sizeof(____fmt), ##__VA_ARGS__); \
  })

/*
 * The __builtin_expect macros are GCC specific macros that use the branch
 * prediction; they tell the processor whether a condition is likely to be true,
 * so that the processor can prefetch instructions on the correct "side" of the
 * branch.
 */
#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

/*
 * Utilities making a small framework over printing debug information
 * While printed messages can be assigned to a ceratin debug level.
 * The message will only be printed if this level is enabled (i.e. entry in a
 * map set).
 */

#define DEBUG_LOW 1
#define DEBUG_HIGH 2

__u8 __always_inline get_debug_lvl(struct bpf_elf_map *DEBUG_MAP) {
  int index = 0;  // the map has size of 1 so index is always 0
  __u8 *value = (__u8 *)bpf_map_lookup_elem(DEBUG_MAP, &index);
  if (!value) {
    return false;
  }
  // bpf_printk("debug level=%d\n", *value);
  return *value;
}

#define bpf_debug_printk2(lvl, fmt, ...)                            \
  ({                                                                \
    if (unlikely(get_debug_lvl(&DEBUG_MAP) == DEBUG_LOW &&          \
                 lvl == DEBUG_LOW)) {                               \
      char ____fmt[] = fmt;                                         \
      bpf_trace_printk(____fmt, sizeof(____fmt), ##__VA_ARGS__);    \
    } else if (unlikely(get_debug_lvl(&DEBUG_MAP) == DEBUG_HIGH)) { \
      char ____fmt[] = fmt;                                         \
      bpf_trace_printk(____fmt, sizeof(____fmt), ##__VA_ARGS__);    \
    }                                                               \
  })

#define bpf_debug_printk(fmt, ...)                               \
  ({                                                             \
    if (unlikely(get_debug_lvl(&DEBUG_MAP))) {                   \
      char ____fmt[] = fmt;                                      \
      bpf_trace_printk(____fmt, sizeof(____fmt), ##__VA_ARGS__); \
    }                                                            \
  })


#define SEND_BACK_SAME_IFACE return bpf_redirect(skb->ifindex, 0)

#endif //HELPERS_H
