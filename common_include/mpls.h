#ifndef MPLS_H
#define MPLS_H

#define MPLS_HLEN 4

/**
 * Have our own version of mpls.h since OracleLinux  does not include newer
 * functionality like encode & decode. sources:
 * https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/mpls.h &
 * https://github.com/torvalds/linux/blob/6f0d349d922ba44e4348a17a78ea51b7135965b1/net/mpls/internal.h
 */

#include <stdbool.h>
#include "bpf_endian.h"
#include "bpf_helpers.h"
#include "helpers.h"

#define BPF_CONTINUE 240

#define MPLS_MAX_STACK_DEPTH 8
#define MPLS_NETPROG_STACK_DEPTH 4
#define MPLS_DEF_TTL 255

#define MPLS_INFO_SID_IDX 0
#define MPLS_INFO_STACK_DEPTH_IDX 1
#define MPLS_INFO_NETPROG_FUNC_IDX 2
#define MPLS_INFO_NETPROG_ARGS_IDX 3

#define IPPROTO_MPLS 137

#define MPLS_LS_LABEL_MASK 0xFFFFF000
#define MPLS_LS_LABEL_SHIFT 12
#define MPLS_LS_TC_MASK 0x00000E00
#define MPLS_LS_TC_SHIFT 9
#define MPLS_LS_S_MASK 0x00000100
#define MPLS_LS_S_SHIFT 8
#define MPLS_LS_TTL_MASK 0x000000FF
#define MPLS_LS_TTL_SHIFT 0

/* Reserved labels */
#define MPLS_LABEL_IPV4NULL 0   /* RFC3032 */
#define MPLS_LABEL_RTALERT 1    /* RFC3032 */
#define MPLS_LABEL_IPV6NULL 2   /* RFC3032 */
#define MPLS_LABEL_IMPLNULL 3   /* RFC3032 */
#define MPLS_LABEL_ENTROPY 7    /* RFC6790 */
#define MPLS_LABEL_GAL 13       /* RFC5586 */
#define MPLS_LABEL_OAMALERT 14  /* RFC3429 */
#define MPLS_LABEL_EXTENSION 15 /* RFC7274 */

#define MPLS_LABEL_FIRST_UNRESERVED 16 /* RFC3032 */

/* Extended Special-Purpose MPLS Label Values
Values 240-255 belong to the 'Experimental' label range acc. RFC7274
*/
#define MPLS_LABEL_SR_NETPROG 255
#define NETPROG_FUNC_LEN 4
#define NETPROG_ARGS_LEN 16

/* Reference: RFC 5462, RFC 3032
 *
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                Label                  | TC  |S|       TTL     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *	Label:  Label Value, 20 bits
 *	TC:     Traffic Class field, 3 bits
 *	S:      Bottom of Stack, 1 bit
 *	TTL:    Time to Live, 8 bits
 */
struct mpls_hdr {
  unsigned int entry;
};

struct mpls_hdrs {
  unsigned int cnt;
  unsigned int hdr[MPLS_MAX_STACK_DEPTH];
};

struct mpls_entry_decoded {
  unsigned int label;
  unsigned char ttl;
  unsigned char tc;
  unsigned char bos;
  // inserted to make -Wpadded happy
  unsigned char align_padding;
};

static inline bool is_eth_p_mpls(unsigned short eth_type);

static inline bool is_ip_p_mpls(unsigned short ip_type);

static inline bool is_mpls_entry_bos(struct mpls_hdr *hdr);

static inline struct mpls_hdr mpls_encode(unsigned int label, unsigned int ttl,
                                          unsigned int tc, bool bos);

static inline struct mpls_entry_decoded mpls_entry_decode(struct mpls_hdr *hdr);

int __always_inline push_mpls_labels2(struct __sk_buff *skb,
                                      struct mpls_hdrs *cached_hdrs);

void __always_inline pop_mpls_labels(struct __sk_buff *skb, unsigned int cnt,
                                     unsigned int stack_depth);

int __always_inline swap_mac_addrs_da(struct __sk_buff *skb);

int __always_inline push_sr_netprog_labels(struct __sk_buff *skb, int loc,
                                           __u8 func, __u16 args);

int __always_inline mpls_traverse_stack(struct __sk_buff *skb, int *hdr_cnt,
                                        unsigned int *sid, __u8 *func,
                                        __u16 *args);

#endif  // MPLS_H
