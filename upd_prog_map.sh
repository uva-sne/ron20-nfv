function get_func_ids(){

	nfv_hdr="common_include/nfv.h"

	for func_id in $(perl -lne 'print "$1=$2" if /(\w+FUNC_ID)\s(\d+)$/' $nfv_hdr);do

		eval $func_id

	done

}

function get_prog_id(){

	name=$1
	prog_id=$(bpftool net show | grep -w ${name} | grep -Eo '[0-9]+$' | sort | head -1)
	echo $prog_id

}

function upd_progmap(){

	BPFTOOL_CMD="bpftool map update pinned /sys/fs/bpf/tc/globals/FUNC_SWITCH_MAP"

	key=$1
	prog_name=$2
	prog_id=$(get_prog_id ${prog_name})

	CMD="$BPFTOOL_CMD key ${key} 0 0 0 value id ${prog_id}"
	eval $CMD 2> /dev/null && echo "Found program ${prog_name} (id=${prog_id})" || echo "Missing program ${prog_name}"

}

get_func_ids

upd_progmap $SR_PROXY_FUNC_ID "proxy_in"
upd_progmap $MPLS_FW_FUNC_ID "mpls_fw"
upd_progmap $MIRROR_FUNC_ID "mirror_f"
upd_progmap $NFV_SWITCH_FUNC_ID "switch"
