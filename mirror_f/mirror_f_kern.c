#include <assert.h>
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <stdbool.h>

#include "bpf_endian.h"
#include "bpf_helpers.h"

#define BPF_LOOP_SUPPORTED

#define DEBUG_MAP MIR_DEBUGS_MAP
#include "common_maps.c"
#include "helpers.h"
#include "mpls.c"
#include "nfv.h"

static_assert(sizeof(struct ethhdr) == ETH_HLEN,
              "ethernet header size does not match.");

/*
 * FUNCTIONS
 */

SEC("mirror_f") int mirror_f1(struct __sk_buff *skb) {
  unsigned int stack_depth = 0, sid = 0;
  __u8 func = 0;
  __u16 args = 0;

#ifdef STANDALONE_MODE
  int ret = mpls_traverse_stack(skb, &stack_depth, &sid, &func, &args);
#else
  int ret = get_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &stack_depth, &sid, &func,
                                &args);
#endif

  if (ret != BPF_CONTINUE) return ret;

  bpf_debug_printk2(DEBUG_LOW, "[mirror] func=%llx args=%llx\n", func, args);

  // 0. Drop the netprog related headers (including the top label as PHP is off)
  pop_mpls_labels(skb, MPLS_NETPROG_STACK_DEPTH, stack_depth);
  // TODO: stack depth should be updated by pop..() function
  stack_depth -= MPLS_NETPROG_STACK_DEPTH;

  // 1. Swap MACs
  swap_mac_addrs_da(skb);

  // 2. clone and xmit it on the interface we received it (This copy is intended
  // to be delivered to the actual recipient)
  ret = bpf_clone_redirect(skb, skb->ifindex, 0);

  if (ret)
    bpf_debug_printk2(DEBUG_HIGH, "[mirror] bpf_clone_redirect err=%d\n", ret);

  // 3. apply MPLS hdr with LOC=arg of previous function

  /*
   * TODO: Currently we do not remove the original destination label, applying
   *       the new label on top of it.
   *       This is currently needed as mpls_traverse_stack() (while receiving
   *       our packet) would stop parsing a MPLS packet which has only a single
   *       BOS label
   *
   *	   Reminder: Remember to set BOS if applying the last label
   *
   */
  // pop_mpls_labels(skb, 1, 1);

  struct mpls_hdrs pushed_hdrs;
  pushed_hdrs.cnt = 1;

  // We use argument from received packet as the SID for deliverying its copy
  unsigned int mirror_top_label = args | 0;

  struct mpls_hdr new_hdr = mpls_encode(mirror_top_label, MPLS_DEF_TTL, 0, 0);
  bpf_memcpy(&pushed_hdrs.hdr[0], &new_hdr, MPLS_HLEN);

  push_mpls_labels2(skb, &pushed_hdrs);
  // TODO: stack depth should be updated by pop..() function
  stack_depth++;

  // 4. Attempt calling sr_proxy locally, if failed send a packet
  unsigned int null = 0;
  ret = save_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &stack_depth,
                             &mirror_top_label, &null, &null);

  if (ret) {
    bpf_debug_printk2(
        DEBUG_LOW, "[mirror] Saving MPLS stack info failed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  bpf_debug_printk2(DEBUG_HIGH, "[mirror] Tail call to func=%d\n",
                    SR_PROXY_FUNC_ID);
  bpf_tail_call(skb, &FUNC_SWITCH_MAP, SR_PROXY_FUNC_ID);

  SEND_BACK_SAME_IFACE;
}

static char _license[] SEC("license") = "GPL";
