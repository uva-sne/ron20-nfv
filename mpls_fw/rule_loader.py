#!/usr/bin/env python3

import pprint
from collections import namedtuple
import collections
import os
import sys

VERBOSE = False


FUNC_MIRROR = 0x4
FUNC_IDS = 0x8

ANY = -1
N_A = -2
NO_MATCH = "0"

PROTO_TCP = 6
PROTO_UDP = 17

VAL_LEN = 64
UPD_KEY_VALUES = collections.defaultdict(dict)

ipv4_rule_fields = 'proto saddr daddr tsport tdport usport udport action action_args'
ipv4_rule = namedtuple('rule', ipv4_rule_fields)

LOC_SHIFT = 44
FUNC_SHIFT = 40


def enc_netprog_params(loc, func, args): return (
    loc << LOC_SHIFT) | (func << FUNC_SHIFT) | args

# TODO: Assign rule_num based on the order of appearance instead of explicetely
# TODO2: Hide the structure of separate port variables per IP protocol


# RULE_NUM	  IPV4PROTO	SADDR       	DADDR	    	TSPORT	TDPORT   USPORT  UDPORT  ACTION		ACT_ARGS
rules = {
    1:	ipv4_rule(PROTO_UDP,	ANY,		ANY,		N_A,	N_A,    ANY,    666,    'netprog_push',	enc_netprog_params(loc=1013, func=FUNC_MIRROR, args=FUNC_IDS)),
    2:	ipv4_rule(PROTO_TCP,	ANY,		ANY,		ANY,	80,     N_A,    N_A,    'accept',	0),
    3:	ipv4_rule(PROTO_TCP,	ANY,		ANY,		ANY,	22,     N_A,    N_A,    'drop',		0),
    4:	ipv4_rule(PROTO_TCP,	ANY,		ANY,		ANY,	23,     N_A,    N_A,    'drop',		0)
}


def get_bin(x, n=0):

    # Source: https://stackoverflow.com/a/21732313/869012
    """
    Get the binary representation of x.

    Parameters
    ----------
    x : int
    n : int
        Minimum number of digits. If x needs less digits in binary, the rest
        is filled with zeros.

    Returns
    -------
    str
    """
    return format(x, 'b').zfill(n)


def pack_rule_action(rules):

    rule_actions_map = {
        'accept': 0,
        'drop': 2,
        'push': 3,
        'netprog_push': 4
    }

    for rule_num in sorted(rules.keys()):
        rule = rules[rule_num]

        # translate action such as 'accept' to a number to be stored in a map
        action_code = rule_actions_map[rule.action]

        # the assumption is we will only use 1B to store this
        if action_code > 255 or action_code < 0:
            raise AssertionError

        # move arguments in order to prepare space for OR with action_code
        action_args = rule.action_args << 8

        action_code_and_args = action_code | action_args
        #print("action_code=%s action_args=%s" % (action_code, action_args))

        UPD_KEY_VALUES['action'][rule_num] = action_code_and_args


def gen_rule_value(rule_num):

    bitstring = [0 for i in range(VAL_LEN)]

    rule_idx = rule_num - 1

    bitstring[rule_idx] = 1

    ret_str = ''.join(reversed(list(map(str, bitstring))))

    ret_int = int(ret_str, 2)

    return ret_int


def get_match_any_rule_nums(rules, rule_field_name):

    any_rules = []

    for rule_num in sorted(rules.keys()):
        rule = rules[rule_num]

        rule_field_key = eval("rule.%s" % rule_field_name)

        if rule_field_key == ANY:
            any_rules.append(rule_num)

    return any_rules


def adjust_any_match_rules(rules):
    for rule_num in sorted(rules.keys()):
        rule = rules[rule_num]

        for rule_field_name in ipv4_rule_fields.split(' '):
            ultimate_value = 0
            for r in get_match_any_rule_nums(rules, rule_field_name):
                ultimate_value = ultimate_value | gen_rule_value(r)
                UPD_KEY_VALUES[rule_field_name][NO_MATCH] = ultimate_value


def pack_rules(rules):

    for rule_num in sorted(rules.keys()):
        rule = rules[rule_num]

        for rule_field_name in ipv4_rule_fields.split(' '):

            # rule actions are created in a separate function 'pack_rule_action'
            if rule_field_name == 'action':
                continue

            rule_field_key = eval("rule.%s" % rule_field_name)

            # rules which are containing ANY match are handled in 'adjust_any_match_rules' function
            if rule_field_key == ANY:
                continue

            # rules which are containing 'N_A' are skipped
            if rule_field_key == N_A:
                continue

            try:

                new_rule_numbers = []
                new_rule_numbers.append(rule_num)
                new_rule_numbers = new_rule_numbers + \
                    get_match_any_rule_nums(rules, rule_field_name)

                for new_rule_num in new_rule_numbers:

                    prev_rule_num = UPD_KEY_VALUES[rule_field_name][rule_field_key]
                    # print UPD_KEY_VALUES
                    prev_rule_num = prev_rule_num

                    curr_rule_num = gen_rule_value(new_rule_num)

                    # bitwise OR
                    rule_num_2upd = prev_rule_num | curr_rule_num
                    ##print('UPDATE rule_field_name=%s, rule_field_key=%s, OR prev=%s | curr=%s, res=%s' % (rule_field_name, rule_field_key, prev_rule_num, curr_rule_num, rule_num_2upd))

                    UPD_KEY_VALUES[rule_field_name][rule_field_key] = rule_num_2upd
                    #print('UPD!', UPD_KEY_VALUES)

            except KeyError:
                rule_val = gen_rule_value(rule_num)
                ##print('CREATE rule_field_name=%s, rule_field_key=%s raw_val=%s val=%s' % (rule_field_name, rule_field_key, rule_num, rule_val))
                UPD_KEY_VALUES[rule_field_name][rule_field_key] = rule_val

    # create a no-match rule
    adjust_any_match_rules(rules)


def print_packed_rules():

    table_format = "%-16s %-32s %-64s %-32s"

    print(table_format % ('rule_field', 'key', 'val_bin', 'val_dec'))
    for rule_field_name in ipv4_rule_fields.split(' '):

        for i in UPD_KEY_VALUES[rule_field_name].keys():

            val = UPD_KEY_VALUES[rule_field_name][i]

            print(table_format % (rule_field_name, i, get_bin(val, 8), val))


def fmt_hex(val, nbytes=1):

    hexval = "%x" % int(val)

    while len(hexval) < 2*nbytes:
        hexval = '%s%s' % ('0', hexval)

    fmt_hexval = ''

    for i in range(0, len(hexval), 2):
        fmt_hexval = fmt_hexval + hexval[i:i+2] + ' '

    return fmt_hexval.strip()


def fmt_value_bpftool(val):
    """
    Generate 8 x 1B int values to be used with `bpftool map update` command
    """

    # change to hex, fill up zeros to 8B
    val_in_hex = format(val, 'x').zfill(16)

    #print('raw_val=%s val_in_hex=%s' % (val, val_in_hex))

    # source: https://stackoverflow.com/questions/9475241/split-string-every-nth-character
    n = 2  # split into groups of two digits
    hex_values_arr = [val_in_hex[i:i+n] for i in range(0, len(val_in_hex), n)]

    int_values_arr = []

    for val in hex_values_arr:
        val_int = int(str(val), 16)
        int_values_arr.append(val_int)

    int_values_arr = [str(i) for i in int_values_arr]

    return ' '.join(int_values_arr)


def run_bpftool_cmd():

    maps_root_dir = '/sys/fs/bpf/tc/globals/'

    rule_maps_d = {
        'action': 'IPT_RULE_ACT_MAP'
    }

    ipv4_maps_d = {
        'proto': 'IPT_IPV4_PROTO_MAP',
        'saddr': 'IPT_IPV4_SADDR_MAP',
        'daddr': 'IPT_IPV4_DADDR_MAP'
    }

    # TODO: TCP and UDP MAPS could be now joined into one dict
    tcp_maps_d = {
        'tsport': 'IPT_TCP_SPORT_MAP',
        'tdport': 'IPT_TCP_DPORT_MAP',
    }

    udp_maps_d = {
        'usport': 'IPT_UDP_SPORT_MAP',
        'udport': 'IPT_UDP_DPORT_MAP',
    }

    bpftool_cmd = "bpftool map update pinned %s key %s value %s"

    map_name = ''
    for rule_field_name in ipv4_rule_fields.split(' '):
        for i in UPD_KEY_VALUES[rule_field_name].keys():

            val = UPD_KEY_VALUES[rule_field_name][i]

            if rule_field_name == 'proto':
                map_name = ipv4_maps_d[rule_field_name]
                bpftool_key = "%s" % i
            elif rule_field_name == 'saddr' or rule_field_name == 'daddr':
                map_name = ipv4_maps_d[rule_field_name]

                addr_splitted = i.replace('.', ' ')

                # quick&dirty workaround to fix key formatting for 'no-match' rule
                if addr_splitted == "0":
                    addr_splitted = "0 0 0 0"

                bpftool_key = addr_splitted

            elif rule_field_name == 'udport' or rule_field_name == 'usport':
                map_name = udp_maps_d[rule_field_name]
                bpftool_key = "hex %s" % fmt_hex(i, nbytes=2)
            elif rule_field_name == 'tdport' or rule_field_name == 'tsport':
                map_name = tcp_maps_d[rule_field_name]
                bpftool_key = "hex %s" % fmt_hex(i, nbytes=2)
            elif rule_field_name == 'action':

                map_name = rule_maps_d[rule_field_name]
                bpftool_key = "%s" % i

            elif rule_field_name == 'action_args':
                # action_args are updated by 'action' part
                pass

            else:
                print('Unmatched field name', rule_field_name)
                continue

            bpftool_value = fmt_value_bpftool(val)

            if map_name:
                cmd = bpftool_cmd % (maps_root_dir + "/" + map_name,
                                     bpftool_key, bpftool_value)
                ret = os.system(cmd)

                exitcode = (ret >> 8) & 0xFF

                if exitcode is not 0:
                    print('Error: ', cmd)
                    sys.exit(1)
                elif VERBOSE is True:
                    print('Exec: ', cmd)

#
#	MAIN
#


if len(sys.argv) == 2:
    if sys.argv[1] == "-v":
        VERBOSE = True
    if sys.argv[1] == "-h":
        print("""Usage: %s [-v|-h]
			-v, verbose mode
			-h, help info

		When run without arguments this script will silently
		attempt to load fw rules""" % sys.argv[0])


pack_rules(rules)
pack_rule_action(rules)

#pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(rules)

if VERBOSE:
    print("")
    print_packed_rules()


run_bpftool_cmd()
