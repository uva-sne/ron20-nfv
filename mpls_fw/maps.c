// TODO: ARRAY might be faster for lookup
struct bpf_elf_map SEC("maps") IPT_RULE_ACT_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    // that could correspond to the number of bits needed to store IPT_MAX_ELEM
    .size_key = sizeof(__u8),
    // this could be only 1B but in order to keep loader code simple I keep it
    // unified for all the maps
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_IPV4_PROTO_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u8),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_IPV4_SADDR_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u32),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_IPV4_DADDR_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u32),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_UDP_SPORT_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u16),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_UDP_DPORT_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u16),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_TCP_SPORT_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u16),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};

struct bpf_elf_map SEC("maps") IPT_TCP_DPORT_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(__u16),
    .size_value = IPT_VALUE_LENGTH,
    .pinning = PIN_GLOBAL_NS,
    .max_elem = IPT_MAX_ELEM,
};
