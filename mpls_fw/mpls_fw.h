#ifndef MPLS_FW_H
#define MPLS_FW_H

#define IPT_VALUE_LENGTH sizeof(__u64)
#define IPT_MAX_ELEM 8 * IPT_VALUE_LENGTH

#define FW_ACT_ACCEPT 0
#define FW_ACT_DROP 2
#define FW_ACT_MPLS_PUSH 3
#define FW_ACT_NETPROG 4

#define NETPROG_LABEL_LOC_SHIFT 44
#define NETPROG_LABEL_FUNC_SHIFT 40

#define NETPROG_LABEL_LOC_MASK 0xFFFFF00000000000
#define NETPROG_LABEL_FUNC_MASK 0x00000F0000000000
#define NETPROG_LABEL_ARGS_MASK 0x000000000000FFFF

/*
 * The Internet Protocol (IP) is defined in RFC 791.
 * The RFC specifies the format of the IP header.
 * In the header there is the IHL (Internet Header Length) field which is 4bit
 * long
 * and specifies the header length in 32bit words.
 * The IHL field can hold values from 0 (Binary 0000) to 15 (Binary 1111).
 * 15 * 32bits = 480bits = 60 bytes
 */
#define MAX_IP_HDR_LEN 60

/*
 * STRUCTS
 */

struct rule_lookup_res {
  __u64 proto;
  __u64 saddr;
  __u64 daddr;
  __u64 sport;
  __u64 dport;
};

// it has to fit in the key of IPT_RULE_ACT_MAP (currently IPT_VALUE_LENGTH
// which is 8B)
struct rule_action_struct {
  __u8 op_code;
  __u64 args;
};

#endif //MPLS_FW_H
