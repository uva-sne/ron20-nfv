#include <assert.h>
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/pkt_cls.h>
#include <linux/tcp.h>
#include <linux/udp.h>

#include "bpf_endian.h"
#include "bpf_helpers.h"
#include "mpls_fw.h"
#include "helpers.h"
#define DEBUG_MAP IPT_DEBUGS_MAP
#include "common_maps.c"
#include "maps.c"
#include "mpls.c"
#include "nfv.h"

static_assert(sizeof(struct ethhdr) == ETH_HLEN,
              "Ethernet header size does not match.");

/*
 * FUNCTIONS
 */

forced_inline int filter(struct __sk_buff *skb, unsigned int hdr_cnt,
                         unsigned int sid) {
  void *data_end = (void *)(long)skb->data_end;
  void *data = (void *)(long)skb->data;

  struct ethhdr *eth = (struct ethhdr *)(data);

  if ((void *)(eth + 1) > data_end) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[fw] Socket buffer struct was malformed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  /*
   *  Accessing IP header
   */
  unsigned int mpls_stack_size = hdr_cnt * MPLS_HLEN;
  struct iphdr *iph = (struct iphdr *)((void *)(eth + 1) + mpls_stack_size);

  if ((void *)(iph + 1) > data_end) {
    bpf_debug_printk2(
        DEBUG_HIGH,
        "[fw] [iphdr] Socket buffer struct was malformed. Packet dropped.\n");
    return TC_ACT_SHOT;
  }

  // multiply ip header by 4 (bytes) to get the number of bytes of the
  // header.
  int iph_len = iph->ihl << 2;
  if (iph_len > MAX_IP_HDR_LEN) {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[fw] IP header is too long: %d. Packet dropped.\n",
                      iph_len);
    return TC_ACT_SHOT;
  }

  __u8 proto = iph->protocol;
  int saddr = iph->saddr;
  int daddr = iph->daddr;
  // this is the key which contains a no-match rule values
  unsigned int no_match_rule_key = 0;

  bpf_debug_printk2(DEBUG_HIGH, "[fw] IP proto: %d\n", proto);
  bpf_debug_printk2(DEBUG_HIGH, "[fw] IP src addr: 0x%x\n", bpf_ntohl(saddr));
  bpf_debug_printk2(DEBUG_HIGH, "[fw] IP dst addr: 0x%x\n", bpf_ntohl(daddr));

  __u64 *rule_num = 0;
  struct rule_lookup_res rule_res = {
      .proto = 0, .saddr = 0, .daddr = 0, .sport = 0, .dport = 0};

  rule_num = bpf_map_lookup_elem(&IPT_IPV4_PROTO_MAP, &proto);

  if (rule_num)
    rule_res.proto = *rule_num;
  else {
    rule_num = bpf_map_lookup_elem(&IPT_IPV4_PROTO_MAP, &no_match_rule_key);
    if (rule_num) rule_res.proto = *rule_num;
  }

  rule_num = bpf_map_lookup_elem(&IPT_IPV4_SADDR_MAP, &saddr);

  if (rule_num)
    rule_res.saddr = *rule_num;
  else {
    rule_num = bpf_map_lookup_elem(&IPT_IPV4_SADDR_MAP, &no_match_rule_key);
    if (rule_num) rule_res.saddr = *rule_num;
  }

  rule_num = bpf_map_lookup_elem(&IPT_IPV4_DADDR_MAP, &daddr);

  if (rule_num)
    rule_res.daddr = *rule_num;
  else {
    rule_num = bpf_map_lookup_elem(&IPT_IPV4_DADDR_MAP, &no_match_rule_key);
    if (rule_num) rule_res.daddr = *rule_num;
  }

  struct udphdr *udph = (struct udphdr *)(void *)(iph + 1);
  struct tcphdr *tcph = (struct tcphdr *)(void *)(iph + 1);

  bpf_debug_printk2(DEBUG_HIGH, "[fw] IPv4 proto rc: %llx\n", rule_res.proto);
  bpf_debug_printk2(DEBUG_HIGH, "[fw] IPv4 saddr rc: %llx\n", rule_res.saddr);
  bpf_debug_printk2(DEBUG_HIGH, "[fw] IPv4 daddr rc: %llx\n", rule_res.daddr);

  switch (iph->protocol) {
    case IPPROTO_UDP:

      if ((void *)(udph + 1) > data_end) {
        bpf_debug_printk2(DEBUG_HIGH,
                          "[fw] [udph] Socket buffer struct was malformed\n");
        return TC_ACT_SHOT;
      }

      // TODO: Add header length check

      __u16 udp_sport = udph->source;
      __u16 udp_dport = udph->dest;

      bpf_debug_printk2(DEBUG_HIGH, "[fw] UDP src port: %d\n",
                        bpf_ntohs(udp_sport));
      bpf_debug_printk2(DEBUG_HIGH, "[fw] UDP dst port: %d\n",
                        bpf_ntohs(udp_dport));

      rule_num = bpf_map_lookup_elem(&IPT_UDP_SPORT_MAP, &udp_sport);
      if (rule_num)
        rule_res.sport = *rule_num;
      else {
        rule_num = bpf_map_lookup_elem(&IPT_UDP_SPORT_MAP, &no_match_rule_key);
        if (rule_num) rule_res.sport = *rule_num;
      }

      rule_num = bpf_map_lookup_elem(&IPT_UDP_DPORT_MAP, &udp_dport);
      if (rule_num)
        rule_res.dport = *rule_num;
      else {
        rule_num = bpf_map_lookup_elem(&IPT_UDP_DPORT_MAP, &no_match_rule_key);
        if (rule_num) rule_res.dport = *rule_num;
      }

      bpf_debug_printk2(DEBUG_HIGH, "[fw] UDP sport rc: %llx\n",
                        rule_res.sport);
      bpf_debug_printk2(DEBUG_HIGH, "[fw] UDP dport rc: %llx\n",
                        rule_res.dport);

      break;

    case IPPROTO_TCP:
      if ((void *)(tcph + 1) > data_end) {
        bpf_debug_printk2(DEBUG_HIGH,
                          "[fw] [tcph] Socket buffer struct was malformed.\n");
        return TC_ACT_SHOT;
      }

      // TODO: Add header length check

      __u16 tcp_sport = tcph->source;
      __u16 tcp_dport = tcph->dest;

      bpf_debug_printk2(DEBUG_HIGH, "[fw] TCP src port: %d\n",
                        bpf_ntohs(tcp_sport));
      bpf_debug_printk2(DEBUG_HIGH, "[fw] TCP dst port: %d\n",
                        bpf_ntohs(tcp_dport));

      rule_num = bpf_map_lookup_elem(&IPT_TCP_SPORT_MAP, &tcp_sport);
      if (rule_num)
        rule_res.sport = *rule_num;
      else {
        rule_num = bpf_map_lookup_elem(&IPT_TCP_SPORT_MAP, &no_match_rule_key);
        if (rule_num) rule_res.sport = *rule_num;
      }

      rule_num = bpf_map_lookup_elem(&IPT_TCP_DPORT_MAP, &tcp_dport);
      if (rule_num)
        rule_res.dport = *rule_num;
      else {
        rule_num = bpf_map_lookup_elem(&IPT_TCP_DPORT_MAP, &no_match_rule_key);
        if (rule_num) rule_res.dport = *rule_num;
      }

      bpf_debug_printk2(DEBUG_HIGH, "[fw] TCP sport rc: %llx\n",
                        rule_res.sport);
      bpf_debug_printk2(DEBUG_HIGH, "[fw] TCP dport rc: %llx\n",
                        rule_res.dport);

      break;

    default:
      // Unknown IP protocol
      bpf_debug_printk2(DEBUG_LOW,
                        "[fw] Unknown IPv4 proto: %d. Packet accepted.\n",
                        iph->protocol);
      // return TC_ACT_OK;
      swap_mac_addrs_da(skb);
      SEND_BACK_SAME_IFACE;
  }

  /*
   * Once the matching pipeline is finished, derive the matched rule number
   * and take action
   *
   * TODO: match most specific rule
   * TODO: implement "ANY" statement for rules
   *
   */

  __u64 matched_rule = rule_res.proto;
  matched_rule &= rule_res.saddr;
  matched_rule &= rule_res.daddr;
  matched_rule &= rule_res.sport;
  matched_rule &= rule_res.dport;

  __u64 lsb = matched_rule;
  lsb = __builtin_bswap64(lsb);

  /*
   * This will choose the rule with the highest priority out of all matched
   * rules e.g. for matched rules 1 and 2, the matched_rules bitmap is 0x03
   * LSB (position) is 1
   */

  __u8 lsb_ = highest_bit(lsb);
  // TODO: Rename lsb_ to indicate that later we treat it as a rule number

  __u64 rule_action = 0;
  unsigned long *res_ = 0;
  struct rule_action_struct rule = {0, 0};

  res_ = bpf_map_lookup_elem(&IPT_RULE_ACT_MAP, &lsb_);

  unsigned long op_code = 0;
  unsigned long op_args = 0;

  if (res_) {
    long int res = __builtin_bswap64(*res_);

    /*
     * Extracting the op_code and op_args
     * Currently we use a 64-bit value to store both.
     * The rule loader (and this code) assume the rule_op is rightmost 1B,
     * the remaining 7B are the rule_args
     */

    op_code = res & 0x00000000000000FF;
    op_args = (res & 0xFFFFFFFFFFFFFF00) >> 8;

    bpf_debug_printk2(DEBUG_HIGH, "[fw] Rule op_code=0x%llx args=0x%llx\n",
                      op_code, op_args);
  } else {
    bpf_debug_printk2(DEBUG_HIGH, "[fw] Rule lookup key=%lld err=0x%llx\n",
                      lsb_, res_);
  }

  if (matched_rule) {
    // rule_action = __builtin_bswap64(*res_);
    rule_action = op_code;

    bpf_debug_printk2(DEBUG_HIGH,
                      "[fw] Matched rules bitmap: 0x%llx, Hi-Prio Rule ID: "
                      "%lld, action=%lld\n",
                      matched_rule, lsb_, rule_action);

    /* Variables to be used within switch statement*/
    struct mpls_hdr new_mpls_hdr;
    struct mpls_hdrs hdr_to_push;

    int loc;
    __u16 func, args;
    /* */

    switch (rule_action) {
      case FW_ACT_ACCEPT:
        bpf_debug_printk2(DEBUG_LOW, "[fw] Action=ACCEPT\n");
        return TC_ACT_OK;
      case FW_ACT_DROP:
        bpf_debug_printk2(DEBUG_LOW, "[fw] Action=DROP\n");
        return TC_ACT_SHOT;
      case FW_ACT_MPLS_PUSH:
        bpf_debug_printk2(DEBUG_LOW, "[fw] Action=MPLS_PUSH, label=%lld\n",
                          op_args);

        /* Preparing the header */
        new_mpls_hdr = mpls_encode(op_args, MPLS_DEF_TTL, 0, false);
        hdr_to_push.cnt = 1;
        bpf_memcpy(&hdr_to_push.hdr[0], &new_mpls_hdr, MPLS_HLEN);

        push_mpls_labels2(skb, &hdr_to_push);
        swap_mac_addrs_da(skb);

        SEND_BACK_SAME_IFACE;

      case FW_ACT_NETPROG:

        /*
        op_args is a 64-bit map value which is populated by rule_loader.py
        The value holds three parameters: location (loc), function (func) and
        function argument (args).
        In case of NETPROG the following bit layout has been used:

        - 0-19 MSB bits encode location
        - 20-23 bits encode function
        - 48-63 bits encode args
        */

        loc = (op_args & NETPROG_LABEL_LOC_MASK) >>
              NETPROG_LABEL_LOC_SHIFT;  // 20 bits
        func = (op_args & NETPROG_LABEL_FUNC_MASK) >>
               NETPROG_LABEL_FUNC_SHIFT;           // 4 bits
        args = op_args & NETPROG_LABEL_ARGS_MASK;  // 16 bits

        hdr_cnt += MPLS_NETPROG_STACK_DEPTH;

        bpf_debug_printk2(
            DEBUG_LOW,
            "[fw] Action=MPLS_NETPROG, loc=%lld func=0x%llx args=0x%llx\n", loc,
            func, args);
        push_sr_netprog_labels(skb, loc, func, args);

        int ret = save_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &hdr_cnt, &loc,
                                       &func, &args);

        if (ret) {
          bpf_debug_printk2(
              DEBUG_LOW,
              "[fw] Saving MPLS stack info failed. Packet dropped.\n");
          return TC_ACT_SHOT;
        }

        /*
         * We only try to perform a tail call if original MPLS packet top label
         * (SID) is equal to the location (loc)
         */

        if (loc == sid) {
          bpf_debug_printk2(DEBUG_LOW, "[fw] Tail call to func=%d\n", func);
          bpf_tail_call(skb, &FUNC_SWITCH_MAP, func);
        }

        /* If the tail call was not successful e.g. there was no NFV locally
         * present the packet is sent out
         */

        swap_mac_addrs_da(skb);
        SEND_BACK_SAME_IFACE;

      default:
        bpf_debug_printk2(DEBUG_HIGH,
                          "[fw] Rule (%lld) action code=0x%llx unknown!\n",
                          lsb_, rule_action);
        return TC_ACT_SHOT;
    }
  } else {
    bpf_debug_printk2(DEBUG_HIGH,
                      "[fw] No match (bitmap=0x%llx), dropping MSB=0x%llx\n",
                      matched_rule, lsb_);
    return TC_ACT_SHOT;
  }
}

// Source: https://stackoverflow.com/a/8462598/869012
forced_inline int highest_bit(long long n) {
  const long long mask[] = {0x000000007FFFFFFF, 0x000000000000FFFF,
                            0x00000000000000FF, 0x000000000000000F,
                            0x0000000000000003, 0x0000000000000001};
  int hi = 64;
  int lo = 0;
  int i = 0;

  if (n == 0) return 0;

#pragma unroll
  for (i = 0; i < sizeof mask / sizeof mask[0]; i++) {
    int mi = lo + (hi - lo) / 2;

    if ((n >> mi) != 0)
      lo = mi;
    else if ((n & (mask[i] << lo)) != 0)
      hi = mi;
  }

  return lo + 1;
}

/*
 * SECTIONS
 */

SEC("mpls_fw") int mpls_fw1(struct __sk_buff *skb) {
  unsigned int hdr_cnt = 0, sid = 0;
  __u8 func;
  __u16 args;

#ifdef STANDALONE_MODE
  int ret = mpls_traverse_stack(skb, &hdr_cnt, &sid, &func, &args);
#else
  int ret =
      get_mpls_stack_info(&FUNC_MPLS_INFO_MAP, &hdr_cnt, &sid, &func, &args);

  bpf_debug_printk2(DEBUG_HIGH,
                    "[fw] get_mpls_stack_info stack depth=%d sid=%d\n", hdr_cnt,
                    sid);
#endif

  if (ret != BPF_CONTINUE) return ret;

  /*
   * Popping a top label
   *
   * TODO: This could be done more efficiently by maybe reading the
   * remaining hdrs and writing them all at once when adding NETPROG hdrs
   */
  pop_mpls_labels(skb, 1, hdr_cnt);
  hdr_cnt--;

#ifdef STANDALONE_MODE
  return filter(skb, hdr_cnt, sid);
#else
  /*
   * This is a shameless workaround to circumvent verifier issues.  We
   * basically assign the value of hdr_cnt to another variable but in a more
   * static manner
   */

  unsigned int hdr_cnt2 = 0;
  for (int i = 0; i < MPLS_MAX_STACK_DEPTH; ++i) {
    // bpf_debug_printk2(DEBUG_LOW, "i=%d\n", i);
    if (hdr_cnt == i) {
      hdr_cnt2 = i;
      break;
    }
  }

  return filter(skb, hdr_cnt2, sid);
#endif
}

static char _license[] SEC("license") = "GPL";
