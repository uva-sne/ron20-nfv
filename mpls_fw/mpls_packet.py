#!/usr/bin/env python

# Based on: https://github.com/NetSys/bess/blob/2516e21348e33be5b65eb9ce451d3bdfce4d4a4b/bessctl/conf/samples/mpls_test.bess

import scapy.all as scapy
import sys

IFACE=sys.argv[1]

# MPLS layer addition to scapy
class MPLS(scapy.Packet):
        name = "MPLS"
        fields_desc =  [
                scapy.BitField("label", 3, 20),
                scapy.BitField("experimental_bits", 0, 3),
                scapy.BitField("bottom_of_label_stack", 1, 1),
                scapy.ByteField("TTL", 255)
        ]
        
scapy.bind_layers(scapy.Ether, MPLS, type = 0x8847)
scapy.bind_layers(MPLS, MPLS, bottom_of_label_stack = 0)
scapy.bind_layers(MPLS, scapy.IP)
        
eth = scapy.Ether(src='02:1e:67:9f:4d:ae', dst='06:16:3e:1b:72:32')
mpls = MPLS(label = 0xDEAD, TTL = 255, bottom_of_label_stack=1)
ip = scapy.IP(src='192.168.0.1', dst='10.0.0.1')
udp = scapy.UDP(sport=10001, dport=10002)

test_packet = (eth/mpls/ip/udp)
scapy.sendp(eth/mpls/ip/udp, count=1, iface=IFACE, verbose=True)
