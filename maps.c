/*
 * This is the map primarly used by sr_proxy, the purpose to have it accessible
 * here is to be able to see if sr_proxy has been configured (we check the map
 * content) to act as a proxy for specific SID
 */

struct bpf_elf_map SEC("maps") PROXY_IN_SID_IF_MAP = {
    .type = BPF_MAP_TYPE_HASH,
    .size_key = sizeof(unsigned int),    // SID
    .size_value = sizeof(unsigned int),  // Interface ID leading towards an NFV
    .pinning = PIN_GLOBAL_NS,
    .max_elem = MAX_NFV_CNT,
};
